import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CoursesService {

  constructor(private http: HttpClient) {
  }

  public getCourseData(): Observable<any> {
    return this.http.get('./assets/testdata/course.stub.json');
  }

}
