import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {SuiModule} from 'ng2-semantic-ui';
import { HeaderComponent } from './header/header.component';
import { TrainingComponent } from './training/training.component';
import { TrainingMonthListComponent } from './training/training-month-list/training-month-list.component';
import { TrainingFilterBarComponent } from './training/training-filter-bar/training-filter-bar.component';
import {CoursesService} from './services/courses.service';
import {CourseDataResolver} from './services/routeprovider.service';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { TrainingOverviewComponent } from './training/training-overview/training-overview.component';
import { MyFilterPipe } from './pipes/my-filter.pipe';

const appRoutes: Routes = [
  {path: '', component: TrainingComponent, resolve: {courses: CourseDataResolver}}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TrainingComponent,
    TrainingMonthListComponent,
    TrainingFilterBarComponent,
    TrainingOverviewComponent,
    MyFilterPipe
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    SuiModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    ),
  ],
  providers: [
    CoursesService,
    CourseDataResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
