import {Pipe, PipeTransform} from '@angular/core';
import {Course} from '../model/courses.model';

@Pipe({
  name: 'myFilter'
})
export class MyFilterPipe implements PipeTransform {

  transform(items: Course[], field: string, value: string): any[] {
    console.log(items);
    console.log(field);
    const returnValue = [];

    if (!items) {
      return [];
    }
    if (!value || value.length === 0) {
      return items;
    }

    for (const item of items) {
      let matchfound = false;
      item.tags.forEach(tag => {
          if (tag.tagname.toLowerCase() === value.toLowerCase()) {
            matchfound = true;
          }
        }
      );
      if (matchfound) {
        returnValue.push(item);
      }
    }
    if (returnValue.length === 0 ) {return items; }
    return Array.from(new Set(returnValue));
  }

}
