export class Course {
  id: number;
  instanceid: number;
  title: string;
  subtitle: string;
  details: string;
  availableplaces: number;
  dates: [{
    datetype: number; // 1=start 2=between 3=end session 4=one day session
    datetime: string;
    sessionname: string;
  }];
  subscriptions: [{
    subscribername: string;
    id: number;
  }];
  tags: [{
    id: number;
    tagname: string;
  }];
}
