import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingFilterBarComponent } from './training-filter-bar.component';

describe('TrainingFilterBarComponent', () => {
  let component: TrainingFilterBarComponent;
  let fixture: ComponentFixture<TrainingFilterBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingFilterBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingFilterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
