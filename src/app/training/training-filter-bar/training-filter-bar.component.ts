import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-training-filter-bar',
  templateUrl: './training-filter-bar.component.html',
  styleUrls: ['./training-filter-bar.component.css']
})
export class TrainingFilterBarComponent implements OnInit {

  @Input() tags: string[];

  @Output() typesearch = new EventEmitter<string>();
  @Output() viewchange = new EventEmitter<string>();
  @Output() invidualdate = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onSearchChange(searchValue: string) {
    this.typesearch.emit(searchValue);
  }

  switchView(event) {
    this.viewchange.emit(event);
  }

  showIndividualDates() {
    this.invidualdate.emit();
  }
}
