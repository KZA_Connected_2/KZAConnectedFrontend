import {Component, Input, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';
import {Course} from '../model/courses.model';
import {TrainingMonthListComponent} from './training-month-list/training-month-list.component';
import {TrainingFilterBarComponent} from './training-filter-bar/training-filter-bar.component';
import {TrainingOverviewComponent} from './training-overview/training-overview.component';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {

  public paddedview = true;
  public courses: Course[];
  public result = [];
  @Input() filterparameter: string;

  @ViewChild(TrainingMonthListComponent) monthlist: TrainingMonthListComponent;
  @ViewChildren(TrainingOverviewComponent) traininglist: QueryList<TrainingOverviewComponent>;
  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.courses = data['courses'];
      }
    );
    this.monthcounter();
    this.tagsetter();
  }

  setfilter(filterstring) {
    this.filterparameter = filterstring;
  }

  changepaddedview() {
    this.paddedview = !this.paddedview;
  }

  // https://stackoverflow.com/questions/40165294/access-multiple-viewchildren-using-viewchild
  changedateview() {
    this.traininglist.forEach(training => {
      training.changeDateView();
    });
  }

  tagsetter() {
    const subarray = [];
    this.courses.forEach(course => {
      course.tags.forEach( tag => {
          subarray.push(tag.tagname);
      }
      );
    });
    this.result = Array.from(new Set(subarray));
  }

  monthcounter() {
    const monthcounterlist = [];
    this.courses.forEach(course => {
        course.dates.forEach(date => {
           monthcounterlist.push(new Date(date.datetime).getMonth());
          }
        );
      }
    );
    const results = new Counter(monthcounterlist);

    const updatearray: [{monthnr: number, count: number}] = [{monthnr: 1, count: 0}];
    for (const [number, times] of results.entries()) {
      updatearray.push({'monthnr': number, 'count': times});
    }
    this.monthlist.updateMonthCount(updatearray);
  }

}

// counter class to implement easy counting
// https://stackoverflow.com/questions/17313268/idiomatically-find-the-number-of-occurrences-a-given-value-has-in-an-array
export class Counter extends Map {
  private key;

  constructor(iter, key= null) {
    super();
    this.key = key || (x => x);
    for (const x of iter) {
      this.add(x);
    }
  }
  add(x) {
    x = this.key(x);
    this.set(x, (this.get(x) || 0) + 1);
  }
}
