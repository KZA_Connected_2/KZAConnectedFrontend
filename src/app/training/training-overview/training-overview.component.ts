import {Component, Input, OnInit} from '@angular/core';
import {Course} from '../../model/courses.model';

@Component({
  selector: 'app-training-overview',
  templateUrl: './training-overview.component.html',
  styleUrls: ['./training-overview.component.css']
})
export class TrainingOverviewComponent implements OnInit {

  @Input() course: Course;
  public showAllDates = false;

  constructor() { }

  ngOnInit() {
  }

  changeDateView() {
    this.showAllDates = !this.showAllDates;
  }
}
