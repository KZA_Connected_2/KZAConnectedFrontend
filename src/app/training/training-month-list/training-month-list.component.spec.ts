import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingMonthListComponent } from './training-month-list.component';

describe('TrainingMonthListComponent', () => {
  let component: TrainingMonthListComponent;
  let fixture: ComponentFixture<TrainingMonthListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingMonthListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingMonthListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
