import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-training-month-list',
  templateUrl: './training-month-list.component.html',
  styleUrls: ['./training-month-list.component.css']
})
export class TrainingMonthListComponent implements OnInit {

  public months = [
    {month: 'januari', count: 0},
    {month: 'februari', count: 0},
    {month: 'maart', count: 0},
    {month: 'april', count: 0},
    {month: 'mei', count: 0},
    {month: 'juni', count: 0},
    {month: 'juli', count: 0},
    {month: 'augustus', count: 0},
    {month: 'september', count: 0},
    {month: 'october', count: 0},
    {month: 'november', count: 0},
    {month: 'december', count: 0}
  ];

  constructor() {
  }

  ngOnInit() {
  }

  public updateMonthCount(values: [{monthnr: number, count: number}]) {
    values.forEach(month => {
          this.months[month.monthnr].count = month.count;
      });
  }
}
